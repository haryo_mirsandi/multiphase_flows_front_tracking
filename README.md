# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* 2D gas-liquid multiphase flows using Front-Tracking type method

### Structure of the repository

- Origin
    * Master

- Instructions
    * Use `git clone <repo link>` to create a copy of the repository on your local machine
    * Use `git fetch && checkout <branch name>` to move to the respective branch ( __the first two steps need to be done once__ )
    * After that you can edit files and use `git status` to check files which are staged(green) or which are untracked (red)
    * To add an untracked file to stage use `git add <file name or wild card>`
    * The files must now show in green color when you do `git status`
    * Use `git commit -m '<your commit message>'` to commit the changes
    * Use `git push -u origin <branch name>` to update the committed changes in your respective branch of the repository.


- Sample operation when you clone the repository
    * Refer to [Cloning a REMOTE Repository with branches](http://stackoverflow.com/questions/67699/clone-all-remote-branches-with-git)
- Happy coding and keep yourself warm and cosy


### Contact Details ###

* Haryo Mirsandi